<?php

use Illuminate\Database\Seeder;

class LogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Logo = new \App\Logo;
        $Logo->logo = 'user.png';
        $Logo->save();
        
        $this->command->info('Logo Berhasil Dibuat');
    }
}
