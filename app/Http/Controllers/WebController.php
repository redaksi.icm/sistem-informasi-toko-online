<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Barang;
use \App\Logo;

class WebController extends Controller
{
    public function index()
    {
        $Barang = Barang::paginate(8);
        $Logo   = Logo::all();
        return view('Public.index',['Data' => $Barang, 'logo' => $Logo]);
    }
}
