<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Logo;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $DataLogo = Logo::all();
        $Logo = Logo::findOrFail($id);
        return view('Admin.Logo.index', ['Data' => $Logo, 'logo' => $DataLogo]);
    }

    public function update(Request $request, $id)
    {
        $Logo = Logo::findOrFail($id);
        if($request->hasFile('logo'))
        {
            $request->file('logo')->move('img/',$request->file('logo')->getClientOriginalName());
            $Logo->logo = $request->file('logo')->getClientOriginalName();
        }
        $Logo->save();

        return redirect()->back()->with('alert-success', 'Logo Berhasil Diubah');
    }
}
