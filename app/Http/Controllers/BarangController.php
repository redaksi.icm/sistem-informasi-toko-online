<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Providers\SweetAlertServiceProvider;

class BarangController extends Controller
{
    public function index()
    {
        $Logo = Logo::all();
        $data = DB::table('items')
                        ->get();

        return view('Admin.Barang.index',['data' => $data, 'logo' => $Logo]);
    }

    public function tbh_barang(Request $req)
    {
        $rules = [
            'nama_barang'   => 'required|unique:items|min:8|bail',
            'stok'          => 'not_in:0|bail',
            'harga'         => 'not_in:0|required|bail',
            'ket'           => 'string|required|min:10|bail',
            'pesan'         => 'string|required|bail',
            'foto_barang'   => 'required|bail'
        ];

        $pesan = [
            'required'      => 'Data Ini Harus Diisi !!!',
            'unique'        => 'Data Ini Sudah Digunakan !!!',
            'min:8'         => 'Data Ini Minimal 8 Karakter !!!',
            'not_in'        => 'Pilih Data Dengan Benar !!!',
            'string'        => 'Data Ini Hanya Bisa Karakter !!!',
            'not_in:0'      => 'Masukan Harga Dengan Benar !!!',
        ];

        $this->validate($req, $rules, $pesan);

        $NamaBarang = $req->get('nama_barang');
        $Harga      = $req->get('harga');
        $Stok       = $req->get('stok');
        $Keterangan = $req->get('ket');
        $Pesan      = $req->get('pesan');
        
        $Barang = new \App\Barang;
        $Barang->nama_barang    = $NamaBarang;
        $Barang->harga_barang   = $Harga;
        $Barang->stok           = $Stok;
        $Barang->keterangan     = $Keterangan;
        $Barang->pesan          = $Pesan;

        if($req->hasFile('foto_barang'))
        {
            $req->file('foto_barang')->move('images/',$req->file('foto_barang')->getClientOriginalName());
            $Barang->foto_barang = $req->file('foto_barang')->getClientOriginalName();
        }
        $Barang->save();

        return redirect('/Barang')->with('success','Ditambahkan')->with('alert-success', $Barang->nama_barang.' Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $Logo = Logo::all();
        $Barang = \App\Barang::findOrFail($id);
        return view('Admin.Barang.edit',['barang' => $Barang, 'logo' => $Logo]);
    }

    public function update(Request $req, $id)
    {
        $Barang = \App\Barang::findOrFail($id);

        $Barang->nama_barang    = $req['nama'];
        $Barang->harga_barang   = $req['harga'];
        $Barang->stok           = $req['stok'];
        $Barang->keterangan     = $req['ket'];
        $Barang->pesan          = $req['pesan'];

        if($req->hasFile('foto_barang'))
        {
            $req->file('foto_barang')->move('img/',$req->file('foto_barang')->getClientOriginalName());
            $Barang->foto_barang = $req->file('foto_barang')->getClientOriginalName();
        }

        $Barang->save();
        
        return redirect()->route('Barang')->with('alert-success', $Barang->nama_barang. ' Berhasil Diubah');
    }

    public function destroy($id)
    {
        $Barang = \App\Barang::findOrFail($id);
        $Barang->delete();

        return redirect()->route('Barang')->with('alert-success', $Barang->nama_barang . 'Berhasil Dihapus');
    }
}
