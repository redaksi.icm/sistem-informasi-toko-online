<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>ADANA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @foreach ($logo as $data)
        <link rel='icon' sizes="96x96" href="{{ asset('img/' . $data->logo) }}">
        <link href="{{ asset('css/public/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('css/public/bootstrap-theme.css') }}" rel="stylesheet">
        <link href="{{ asset('css/public/default.css') }}" rel="stylesheet">
        <link href="{{ asset('css/public/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/public/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <section id="header" class="appear"></section>
            <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:100px; height:90px; background-color:rgba(0,0,0,0.3);" data-300="line-height:10px; height:20px; background-color:rgba(0,0,0,1);">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="fa fa-bars color-white"></span>
                        </button>
                        <h1><a class="navbar-brand" href="{{ route('Beranda') }}" data-0="line-height:80px;" data-300="line-height:30px;">ADANA</a></h1>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav" data-0="margin-top:20px;" data-300="margin-top:5px;">
                            <li><a href="{{ route('Beranda') }}">Beranda</a></li>
                            <li><a href="#barang">Barang</a></li>
                            <li><a href="#tentang">Tentang</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        <section class="featured">
            <div class="container">
                <div class="row mar-bot40">
                    <div class="align-center">
                        <center><img src="{{ asset('img/' . $data->logo) }}" class="img-responsive mr-auto ml-auto"></center>
                        @endforeach
                        <h3 class="slogan">ADANA</h3>
                    </div>
                </div>
            </div>
        </section>
        <br>
        
        <section id="parallax1" class="section">
        </section>

        <!-- Section List Items Jas -->
        <section id="barang" class="section appear clearfix">
            <div class="container">
                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="section-header">
                            <h2 class="section-heading animated" data-animation="bounceInUp">Barang</h2>
                            <p>Daftar barang dagangan yang akan dijual di ADANA.</p>
                        </div>
                    </div>
                </div>
                <!-- col-md-12 Items -->
                <div class="col-md-12">
                    <!-- row Items -->
                    <div class="row">
                        @foreach ($Data as $dataBarang)
                        <div class="col-md-3">
                            <div class="card col-md-12">
                                <div class="card-body">
                                    @if($dataBarang->stok == 'ada')
                                    <div class="row">
                                        <img class="img-thumbnail img-responsive img-container" src=" {{ asset('img/' . $dataBarang->foto_barang) }} "/>
                                        <h5 class="thumbnail-nama">{{ $dataBarang->nama_barang }}</h5>
                                        <h5 class="thumbnail-harga">Rp. {{ number_format($dataBarang->harga_barang) }}</h5>
                                        <h5 class="thumbnail-stok">Ada</h5>
                                    </div>
                                    <div class="row">
                                        <a href="https://api.whatsapp.com/send?phone=+6281295203941&text=Haloo Adana Shop, {{ $dataBarang->pesan }}" class="btn btn-info col-md-12" target="_blank">Beli</a>
                                    </div>
                                    @elseif ($dataBarang->stok == 'habis')
                                    <div class="row">
                                        <img class="img-thumbnail-habis img-responsive img-container" src=" {{ asset('img/' . $dataBarang->foto_barang) }} "/>
                                        <h5 class="thumbnail-nama-habis">{{ $dataBarang->nama_barang }}</h5>
                                        <h5 class="thumbnail-harga-habis">Rp. {{ number_format($dataBarang->harga_barang) }}</h5>
                                        <h5 class="thumbnail-stok-habis">Habis</h5>
                                    </div>
                                    <div class="row">
                                        <a href="#" class="btn btn-info col-md-12 disabled" target="_blank">Beli</a>
                                    </div>
                                    @endif
                                </div>      
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="text-center">
                            {{$Data->appends(Request::all())->links()}}
                        </div>
                    </div>
                    <!-- End row Items -->
                </div>
                <!-- End col-md-12 Items -->
            </div>
        </section>
        <!-- End Section -->

        
        <section id="testimonials" class="section" data-stellar-background-ratio="0.5">
        </section>

        <!-- Tentang -->
        <section id="tentang" class="section appear clearfix">
            <div class="container">
                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                    <div class="section-header">
                        <h2 class="section-heading animated" data-animation="bounceInUp">Our Team</h2>
                        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
                    </div>
                    </div>
                </div>

                <div class="row align-center mar-bot40">
                    <div class="col-md-12">
                     <div class="row">
                       <div class="col-md-6">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi inventore aspernatur a laborum ea praesentium dolor sequi, blanditiis accusamus fugit qui architecto eum necessitatibus consequatur provident? Illum praesentium sapiente sed.
                        </div>
                        <div class="col-md-6">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi inventore aspernatur a laborum ea praesentium dolor sequi, blanditiis accusamus fugit qui architecto eum necessitatibus consequatur provident? Illum praesentium sapiente sed.
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Tentang -->

        <section id="footer" class="section footer">
            <div class="container">
                <div class="row animated opacity mar-bot20" data-andown="fadeIn" data-animation="animation">
                    <div class="col-sm-12 align-center">
                    <ul class="social-network social-circle">
                        <li><a href="#" class="icoRss" title="Rss"><i class="fab fa-whatsapp"></i></a></li>
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                    </div>
                </div>

                <div class="row align-center copyright">
                    <div class="col-sm-12">
                        <p>Copyright &copy; ADANA</p>
                    </div>
                </div>
            </div>
        </section>
        <a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>


    <script src="{{ asset('js/public/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
    <script src="{{ asset('js/public/jquery.js') }}"></script>
    <script src="{{ asset('js/public/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('js/public/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/public/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('js/public/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('js/public/fancybox/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/public/skrollr.min.js') }}"></script>
    <script src="{{ asset('js/public/jquery.scrollTo.js') }}"></script>
    <script src="{{ asset('js/public/jquery.localScroll.js') }}"></script>
    <script src="{{ asset('js/public/stellar.js') }}"></script>
    <script src="{{ asset('js/public/jquery.appear.js') }}"></script>
    <script src="{{ asset('js/public/main.js') }}"></script>
    <script src="https://kit.fontawesome.com/20e16e5617.js"></script>
    </body>

</html>