<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Admin = new \App\User;
        $Admin->username = 'Admin';
        $Admin->password = \Hash::make("adana123");
        $Admin->save();
        
        $this->command->info('User Admin Berhasil Dibuat');
    }
}
