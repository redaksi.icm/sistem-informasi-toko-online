<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'WebController@index');
Route::get('/Beranda','WebController@index')->name('Beranda');

Route::get('/Dasbor','LoginController@index');
Route::get('/Login', 'LoginController@showloginadmin')->name('Login');
Route::post('/LoginPost','LoginController@loginPost')->name('LoginPost');
Route::get('/logout','LoginController@logout');

Route::group(['middleware' => 'auth'], function() {

    Route::get('/Admin','LoginController@beranda')->name('Admin');

    // Logo
    Route::get('Logo/{id}', 'LogoController@index');
    Route::post('UpdateLogo/{id}', 'LogoController@update')->name('UpdateLogo');

    // Barang
    Route::get('/Barang', 'BarangController@index')->name('Barang');
    Route::post('/TambahBarang', 'BarangController@tbh_barang')->name('TambahBarang');
    Route::get('/HapusBarang/{id}', 'BarangController@destroy')->name('HapusBarang');
    Route::get('/EditBarang/{id}', 'BarangController@edit')->name('EditBarang');
    Route::post('/UpdateBarang/{id}', 'BarangController@update');
});