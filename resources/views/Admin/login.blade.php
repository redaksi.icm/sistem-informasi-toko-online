<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ADANA</title>

    <!-- Bootstrap -->
    <link href="{{asset('css/admin/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('css/admin/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{asset('css/admin/custom.min.css')}}" rel="stylesheet">

    </head>

    <body class="login">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        @if(\Session::has('alert'))
                            <div class="alert alert-danger">
                                <div>{{Session::get('alert')}}</div>
                            </div>
                        @endif
                        @if(\Session::has('alert-success'))
                            <div class="alert alert-success">
                                <div>{{Session::get('alert-success')}}</div>
                            </div>
                        @endif
                        
                        <form action="{{ route('LoginPost') }}" method="POST" autocomplete="off">
                            @csrf
                            <h1><i class="fa fa-building"></i> ADANA</h1>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Masukan Nama User" required="" />
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Masukan Kata Sandi" required="" />
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-md btn-primary">Masuk</button>
                            </div>
                            <div class="separator">

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>

        <script src="{{asset('js/admin/jquery.min.js')}}"></script>
        <script src="{{asset('js/admin/bootstrap.bundle.min.js')}}"></script>

    </body>
</html>
<script>
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                $(this).remove(); 
            });
        }, 2000);
    });
</script>