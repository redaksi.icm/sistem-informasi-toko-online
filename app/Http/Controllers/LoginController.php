<?php

namespace App\Http\Controllers;

use App\ModelUser;
use App\Logo;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Redirector;

class LoginController extends Controller
{
    public function index()
    {
        if(!Session::get('login')){
            return redirect()->route('login')->with('alert','Anda harus login dulu !!!');
        }
        else{
            return redirect('index');
        }
    }

    public function showloginadmin(){
        return view('Admin.login');
    }

    public function loginPost(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $data = ModelUser::where('username',$username)->first();
        if($data != null) //cek username ada atau tidak
        { 
            if(Auth::attempt(['username' => $username, 'password' => $password]))
            {
                return redirect()->route('Admin')->with('alert-success','Selamat Datang '.$data->username. ' di ADANA');
            }
            else
            {
                return redirect()->back()->with('alert','Password Anda Salah!!!');
            }
        }
        else{
            return redirect()->back()->with('alert','Username Anda Salah!!!');
        }
    }

    public function beranda()
    {
        $Logo = Logo::all();
        return view('Admin.index', ['logo' => $Logo]);
    } 

    public function logout(){
        Session::flush();
        return redirect()->route('Login')->with('alert-success','Kamu Berhasil Logout');
    }
}
